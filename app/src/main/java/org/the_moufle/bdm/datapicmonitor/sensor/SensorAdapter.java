package org.the_moufle.bdm.datapicmonitor.sensor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.the_moufle.bdm.datapicmonitor.R;

import java.util.List;

public class SensorAdapter extends BaseAdapter {

    private List<Sensor> sensorList;
    private LayoutInflater inflater;
    private Context context;
    private static final String TAG = "SensorAdapter";
    private SensorManager sensorManager;

    public SensorAdapter(Context context, List<Sensor> sensorList, SensorManager sensorManager) {
        this.context        = context;
        this.sensorList     = sensorList;
        inflater            = LayoutInflater.from(this.context);
        this.sensorManager  = sensorManager;
    }

    @Override
    public int getCount() {
        return sensorList.size();
    }

    @Override
    public Object getItem(int position) {
        return sensorList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FrameLayout layoutItem;

        if (convertView == null) {
            layoutItem = (FrameLayout) inflater.inflate(R.layout.sensor, parent, false);
        } else {
            layoutItem = (FrameLayout) convertView;
        }

        TextView sensorLabel = (TextView)layoutItem.findViewById(R.id.sensor_label);
        TextView sensorValue = (TextView)layoutItem.findViewById(R.id.sensor_value);
        TextView sensorUnit  = (TextView)layoutItem.findViewById(R.id.sensor_unit);

        try {

            sensorLabel.setText(sensorList.get(position).getLabel());
            sensorUnit.setText(sensorList.get(position).getUnitSymbol());

            Button removeSensor  = (Button)layoutItem.findViewById(R.id.remove_sensor_button);

            final int localPosition = position;

            removeSensor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sensorManager.removeSensor(sensorList.get(localPosition));
                }
            });

            Sensor currentSensor    = sensorList.get(position);
            Activity activity       = (Activity) context;
            int fragmentWidth       = activity.findViewById(R.id.bt_fragment).getWidth();
            Double ratio            = fragmentWidth / currentSensor.getLimitMax();
            Double maxSensorValue   = currentSensor.getLimitMax();

            String sCurrentValue    = currentSensor.getCurrentValue().toString();
            Double dCurrentValue    = Double.valueOf(sCurrentValue);
            Double barWidth         = dCurrentValue * ratio;
            int barWidthRounded     = barWidth.intValue() - 32; // Minus left and right default Android margin

            sensorValue.setText(sCurrentValue);

            dCurrentValue = (dCurrentValue / maxSensorValue) * 100;

            int threshold = 60;

            RelativeLayout sensorBackground = (RelativeLayout) layoutItem.findViewById(R.id.sensor_backgound);
            sensorBackground.setBackgroundColor(currentSensor.getColor());
            sensorBackground.requestLayout();
            Double alpha = 0.0;

            if (dCurrentValue > threshold) {
                alpha = ((dCurrentValue - threshold) / ((100-threshold)));
            }

            sensorBackground.setAlpha(alpha.floatValue());
            sensorBackground.getLayoutParams().width = barWidthRounded;

    //      Log.i(TAG, "Screen width = " + size.x + ", sValue = " + sensorList.get(position).getCurrentValue().toString() + ", Limit = " + sensorList.get(position).getLimitMax() + ", Ratio = " + ratio + ", barWidth = " + String.valueOf(barWidthRounded));
    //      layoutItem.requestLayout();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return layoutItem;
    }


}
