package org.the_moufle.bdm.datapicmonitor.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.the_moufle.bdm.datapicmonitor.R;
import org.the_moufle.bdm.datapicmonitor.sensor.SensorDTO;

import java.util.ArrayList;
import java.util.List;

public class AddSensorActivity extends Activity {

    private List<SensorDTO> sensorDTOList = new ArrayList<>();
    private ArrayAdapter<SensorDTO> sensorListAdapter;

    /**
     * Return Intent extra
     */
    public static String SENSOR_LABEL       = "sensor_label";
    public static String SENSOR_DATA_KEY    = "sensor_data_key";
    public static String SENSOR_LIMIT_MIN   = "sensor_limit_min";
    public static String SENSOR_LIMIT_MAX   = "sensor_limit_max";
    public static String SENSOR_UNIT_SYMBOL = "sensor_unit_symbol";
    public static String SENSOR_COLOR       = "sensor_color";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sensor);

        ListView sensorItemList = (ListView) findViewById(R.id.sensor_list);
        sensorListAdapter = new ArrayAdapter<SensorDTO>(this, R.layout.sensor_list_item, sensorDTOList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                RelativeLayout layoutItem;

                if (convertView == null) {
                    layoutItem = (RelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.sensor_list_item, parent, false);
                } else {
                    layoutItem = (RelativeLayout) convertView;
                }

                TextView sensorItemLabel = (TextView)layoutItem.findViewById(R.id.sensor_item_label);
                sensorItemLabel.setText(sensorDTOList.get(position).sensorLabel);

                return layoutItem;
            }
        };
        sensorItemList.setAdapter(sensorListAdapter);
        sensorItemList.setOnItemClickListener(sensorItemClickListener);

        sensorListAdapter.add(new SensorDTO("OilT", "engineOilT", 70.0, 145.0, "C", 0xFFFF0000));
        sensorListAdapter.add(new SensorDTO("OilP", "engineOilP", 2.0, 9.0, "B", 0xFFFF0000));
        sensorListAdapter.add(new SensorDTO("WaterT", "engineWaterT", 60.0, 110.0, "C", 0xFFFF0000));
        sensorListAdapter.add(new SensorDTO("BoostP", "engineBoostP", 0.0, 1.5, "B", 0xFFFF0000));
    }

    private AdapterView.OnItemClickListener sensorItemClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            SensorDTO sensorDTO = sensorListAdapter.getItem(av.getPositionForView(v));

            Intent intent = new Intent();
            intent.putExtra(SENSOR_LABEL,       sensorDTO.sensorLabel);
            intent.putExtra(SENSOR_DATA_KEY,    sensorDTO.sensorDataKey);
            intent.putExtra(SENSOR_LIMIT_MIN,   sensorDTO.sensorLimitMin);
            intent.putExtra(SENSOR_LIMIT_MAX,   sensorDTO.sensorLimitMax);
            intent.putExtra(SENSOR_UNIT_SYMBOL, sensorDTO.sensorUnitSymbol);
            intent.putExtra(SENSOR_COLOR,       sensorDTO.sensorColor);

            setResult(RESULT_OK, intent);
            finish();
        }
    };
}
