package org.the_moufle.bdm.datapicmonitor.DataGenerator;

import android.os.Handler;

import org.the_moufle.bdm.datapicmonitor.bluetooth.BluetoothConstants;

import java.text.DecimalFormat;
import java.util.Random;

public class DataGeneratorT extends Thread {

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    private Handler handler;
    private int oilTemp             = 100;
    private double oilPressure      = 8.5;
    private int waterTemp           = 80;
    private double boostPressure    = 0.6;
    private static final String TAG = "DataGeneratorT";
    private DecimalFormat doubleFormater ;

    public DataGeneratorT(Handler handler) {
        this.handler   = handler;
        doubleFormater = new java.text.DecimalFormat("0.#");
    }

    private int updateIntRandomlyBetweenLimits(int min, int max, int currentValue) {
        int value = currentValue;

        if (new Random().nextBoolean()) {
            value++;
        } else {
            value--;
        }
        return (value > min && value < max) ? value : currentValue;
    }

    private double updateDoubleRandomlyBetweenLimits(double min, double max, double currentValue) {
        double value = currentValue;

        if (new Random().nextBoolean()) {
            value = value + 0.5;
        } else {
            value = value - 0.5;
        }
        return (value > min && value < max) ? value : currentValue;
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            oilTemp         = updateIntRandomlyBetweenLimits(80, 150, oilTemp);
            waterTemp       = updateIntRandomlyBetweenLimits(60, 115, waterTemp);
            oilPressure     = updateDoubleRandomlyBetweenLimits(7, 9, oilPressure);
            boostPressure   = updateDoubleRandomlyBetweenLimits(0, 1.2, boostPressure);

            String s    =
                    "{" +
                    "engineOilT:" + oilTemp +
                    ", engineWaterT:" + waterTemp +
                    ", engineOilP:" + doubleFormater.format(oilPressure).replace(',', '.') +
                    ", engineBoostP:" + doubleFormater.format(boostPressure).replace(',', '.')  +
                    "}";
            handler.obtainMessage(BluetoothConstants.MESSAGE_READ, s).sendToTarget();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
            }
        }
    }
}
