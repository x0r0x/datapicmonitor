package org.the_moufle.bdm.datapicmonitor.sensor;

public class SensorDTO {
    public String sensorLabel;
    public String sensorDataKey;
    public Double sensorLimitMin;
    public Double sensorLimitMax;
    public String sensorUnitSymbol;
    public int    sensorColor;

    public SensorDTO(
            String sensorLabel,
            String sensorDataKey,
            Double sensorLimitMin,
            Double sensorLimitMax,
            String sensorUnitSymbol,
            int    sensorColor
    ) {
        this.sensorLabel        = sensorLabel;
        this.sensorDataKey      = sensorDataKey;
        this.sensorLimitMin     = sensorLimitMin;
        this.sensorLimitMax     = sensorLimitMax;
        this.sensorUnitSymbol   = sensorUnitSymbol;
        this.sensorColor        = sensorColor;
    }
}
