package org.the_moufle.bdm.datapicmonitor;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.the_moufle.bdm.datapicmonitor.DataGenerator.DataGeneratorFragment;
import org.the_moufle.bdm.datapicmonitor.DataGenerator.DataGeneratorT;
import org.the_moufle.bdm.datapicmonitor.activities.AddSensorActivity;
import org.the_moufle.bdm.datapicmonitor.activities.DeviceListActivity;
import org.the_moufle.bdm.datapicmonitor.bluetooth.BluetoothConstants;
import org.the_moufle.bdm.datapicmonitor.sensor.Sensor;
import org.the_moufle.bdm.datapicmonitor.sensor.SensorManager;

import java.util.Iterator;

public class DataPicMonitorMainActivity extends Activity {

    private SensorManager sensorManager;
    private Handler sensorDisplayHandler;
    private SharedPreferences sharedPref;
    private DataGeneratorFragment dataGeneratorFragment;

    private static final String TAG             = "DataPicMonitorMain";
    public final static int DEVICE_LIST_REQUEST = 900;
    public final static int ADD_SENSOR_REQUEST  = 899;
    public final static int REQUEST_ENABLE_BT   = 800;

    public Handler getHandler() {
        return sensorDisplayHandler;
    }

    public SensorManager getSensorManager() {
        return sensorManager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        sensorManager= new SensorManager(this);

        sensorDisplayHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case BluetoothConstants.MESSAGE_READ:
                        try {
                            JSONObject jsonValues = new JSONObject(String.valueOf(msg.obj));
                            Iterator receivedKeys = jsonValues.keys();
                            while(receivedKeys.hasNext()) {
                                String key = receivedKeys.next().toString();
                                sensorManager.updateSensorValue(key, jsonValues.get(key));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "Handler::" + e.toString());
                        }
                }
            }
        };


//        sharedPref  = getPreferences(Context.MODE_PRIVATE);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String jsonConfigFromString = sharedPref.getString("sensorsConf", "conf_empty");
        Log.d(TAG, "Loaded with conf : " + sharedPref.getAll().toString());

//        Data Format
//        String json =   "{engineOilT:{label:\"Oil T\", limitMin:90, limitMax:140, unitSymbol:'C'}," +
//                "engineWaterT:{label:\"Water\", limitMin:80, limitMax:110, unitSymbol:'C'}," +
//                "engineOilP:{label:\"Oil P\", limitMin:2, limitMax:9, unitSymbol:'B'}," +
//                "engineBoostP:{label:\"Boost\", limitMin:0, limitMax:1.3, unitSymbol:'B'}" +
//                "}";

//        BluetoothAdapter btAdapter   = BluetoothAdapter.getDefaultAdapter();
//
//        if (btAdapter == null) {
//            FragmentActivity activity = getActivity();
//            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
//            activity.finish();
//        }
//
//        if (!btAdapter.isEnabled()) {
//            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
//        }
//
//        setupBT();

        try {
            if (jsonConfigFromString.compareTo("conf_empty") != 0) {

                JSONObject jsonObject = new JSONObject(jsonConfigFromString);
                sensorManager.loadFromJson(jsonObject);
            }
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }

        Button addSensorButton = (Button) findViewById(R.id.add_sensor_button);

        addSensorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addSensorIntent = new Intent(getApplicationContext(), AddSensorActivity.class);
                startActivityForResult(addSensorIntent, ADD_SENSOR_REQUEST);
            }
        });

        ImageButton settingsButton = (ImageButton) findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent btConnectIntent = new Intent(getParent(), DeviceListActivity.class);
//                startActivityForResult(btConnectIntent, DEVICE_LIST_REQUEST);
            }
        });
//
//        ListView sensorList = (ListView) findViewById(R.id.sensorsListView);
//
//        sensorList.setAdapter(sensorManager.getSensorAdapter());

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        dataGeneratorFragment = new DataGeneratorFragment();
        transaction.add(0, dataGeneratorFragment);
        transaction.commit();

        transaction = getFragmentManager().beginTransaction();
        Fragment displayFragment = new VerticalSensorsPanelFragment();
        transaction.add(R.id.sensors_display_layout, displayFragment);
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();

        DataGeneratorT t = dataGeneratorFragment.getDataGenerator();
        t.setHandler(sensorDisplayHandler);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode)
        {
            case DEVICE_LIST_REQUEST:
                try {
                    String btMacAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    if (btMacAddress != null) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.bt_device_address_key), btMacAddress);
                        editor.apply();
                        btConnect(btMacAddress);
                    }
                } catch (Exception e) {
                }
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    setupBT();
                } else {
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,Toast.LENGTH_SHORT).show();
                    finish();
                }
            case ADD_SENSOR_REQUEST:
                try {
                    sensorManager.addSensor(new Sensor(
                            data.getExtras().getString(AddSensorActivity.SENSOR_LABEL),
                            data.getExtras().getString(AddSensorActivity.SENSOR_DATA_KEY),
                            data.getExtras().getDouble(AddSensorActivity.SENSOR_LIMIT_MIN),
                            data.getExtras().getDouble(AddSensorActivity.SENSOR_LIMIT_MAX),
                            data.getExtras().getString(AddSensorActivity.SENSOR_UNIT_SYMBOL),
                            data.getExtras().getInt(AddSensorActivity.SENSOR_COLOR)
                    ));
                } catch (NullPointerException e) {
                    // @TODO : check why Null on data.getExtras (when activity ends not normally)
                }

        }
    }

    public void setupBT() {
        String btMacAddress = sharedPref.getString(getString(R.string.bt_device_address_key), "");
        if (!btMacAddress.isEmpty()) {
            btConnect(btMacAddress);
        } else {
            startActivityForResult(new Intent(getParent(), DeviceListActivity.class), DEVICE_LIST_REQUEST);
        }
    }

    public void btConnect(String macAddress){
//        final BluetoothDevice device = btAdapter.getRemoteDevice(macAddress);
//        BTService btService = new BTService();
//        btService->connect(device);
//        Toast.makeText(getActivity(), "Connected to " + macAddress, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "ENDED with conf " + sensorManager.getJson().toString());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("sensorsConf", sensorManager.getJson().toString());
        editor.apply();
//        btAdapter.disable();
        super.onDestroy();
    }
}

