package org.the_moufle.bdm.datapicmonitor.DataGenerator;

import android.app.Fragment;
import android.os.Bundle;

import org.the_moufle.bdm.datapicmonitor.DataPicMonitorMainActivity;

public class DataGeneratorFragment extends Fragment {

    private DataGeneratorT dataGeneratorT;

    public DataGeneratorT getDataGenerator() {
        return dataGeneratorT;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        DataPicMonitorMainActivity parentActivity = (DataPicMonitorMainActivity) getActivity();

        dataGeneratorT = new DataGeneratorT(parentActivity.getHandler());
        dataGeneratorT.start();
    }
}
