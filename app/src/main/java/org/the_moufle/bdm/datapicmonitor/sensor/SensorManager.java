package org.the_moufle.bdm.datapicmonitor.sensor;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SensorManager
{
    /**
     *  sensorCollection List of sensors Objects
     */
    private List<Sensor>         sensorCollection = null;
    /**
     * sensorHashMap Key : sensor key string, Value : corresponding sensor object position in sensorCollection
     */
    private Map<String, Integer> sensorHashMap = new HashMap<String, Integer>();

    private JSONObject           configuration;
    private static final String  TAG = "SensorManager";
    private SensorAdapter        sensorAdapter;

    public SensorManager(Context context) {
        this.configuration    = new JSONObject();
        this.sensorCollection = new ArrayList<Sensor>();
        this.sensorAdapter    = new SensorAdapter(context, this.sensorCollection, this);
    }

    public SensorAdapter getSensorAdapter() {
        return sensorAdapter;
    }

    public Boolean addSensor(Sensor s) {
        if (!sensorHashMap.containsKey(s.getDataKey())) {
            try {
                JSONObject sensorToJson = new JSONObject();
                sensorToJson.put("label", s.getLabel());
                sensorToJson.put("limitMin", s.getLimitMin());
                sensorToJson.put("limitMax", s.getLimitMax());
                sensorToJson.put("unitSymbol", s.getUnitSymbol());
                sensorToJson.put("color", s.getColor());
                configuration.put(s.getDataKey(), sensorToJson);

                sensorCollection.add(s);
                sensorHashMap.put(s.getDataKey(), sensorCollection.indexOf(s));

                return true;
            } catch (JSONException e) {
                return false;
            }
        }
        return false;
    }

    public void removeSensor(Sensor s) {
        configuration.remove(s.getDataKey());
        sensorHashMap.remove(s.getDataKey());
        int sensorIndex = sensorCollection.indexOf(s);
        sensorCollection.remove(s);
        reindexSensorHashMap(sensorIndex);
        sensorAdapter.notifyDataSetChanged();
    }

    private void reindexSensorHashMap(int startIndex)
    {
        for(;startIndex<sensorCollection.size();startIndex++) {
            String sensorDataKey = sensorCollection.get(startIndex).getDataKey();
            if (sensorHashMap.containsKey(sensorDataKey)) {
                sensorHashMap.put(sensorDataKey, startIndex);
            }
        }
    }

    public List<Sensor> getSensorCollection() {
        return sensorCollection;
    }

    public void updateSensorValue(String key, Object value) throws Exception {
        if (sensorHashMap.containsKey(key)) {
            Integer index = sensorHashMap.get(key);
            if (index != null) {
                sensorCollection.get(index).setCurrentValue(value);
                sensorAdapter.notifyDataSetChanged();
            }
        }
    }

    public void loadFromJson(JSONObject jsonConfig) {
        sensorCollection.removeAll(sensorCollection);
        Iterator jsonObjectIterator = jsonConfig.keys();

        while(jsonObjectIterator.hasNext()) {
            try {
                String dataKey = jsonObjectIterator.next().toString();
                JSONObject sensorConfig = jsonConfig.getJSONObject(dataKey);
                addSensor(new Sensor(
                        sensorConfig.get("label").toString(),
                        dataKey,
                        Double.valueOf(sensorConfig.get("limitMin").toString()),
                        Double.valueOf(sensorConfig.get("limitMax").toString()),
                        sensorConfig.get("unitSymbol").toString(),
                        Integer.valueOf(sensorConfig.get("color").toString())
                ));
            } catch (JSONException e) {
                Log.e(TAG, e.toString());
            }
        }
    }

    public JSONObject getJson() {
        return configuration;
    }

    public String dumpSensors() {

        StringBuilder s = new StringBuilder();
        for(int i=0; i<sensorCollection.size(); i++) {
            s.append(sensorCollection.get(i).toString());
        }
        return s.toString();
    }
}
