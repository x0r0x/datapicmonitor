package org.the_moufle.bdm.datapicmonitor;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.the_moufle.bdm.datapicmonitor.adapters.RecyclerListAdapter;
import org.the_moufle.bdm.datapicmonitor.helper.OnStartDragListener;
import org.the_moufle.bdm.datapicmonitor.helper.SimpleItemTouchHelperCallback;
import org.the_moufle.bdm.datapicmonitor.sensor.SensorAdapter;
import org.the_moufle.bdm.datapicmonitor.sensor.SensorManager;

public class VerticalSensorsPanelFragment extends Fragment { //implements OnStartDragListener {

    private static final String TAG = "VerticalSensorsPanelFragment";

//    private ItemTouchHelper mItemTouchHelper;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v              =  inflater.inflate(R.layout.vertical_sensor_panel_fragment, container, false);
        ListView sensorList = (ListView) v.findViewById(R.id.sensorsListView);

        sensorList.setAdapter(((DataPicMonitorMainActivity)getActivity()).getSensorManager().getSensorAdapter());

        return v;
    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);



////        RecyclerView view = (RecyclerView) inflater.inflate(R.layout.vertical_sensor_panel_fragment, container, false);
////
//        final ListView sensorList = (ListView) view.findViewById(R.id.sensorsListView);
//////        sensorList.setAdapter(sManager.getSensorAdapter(sensorList));
////
////        return view;
//        return new RecyclerView(container.getContext());
//    }

//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        final ListView sensorList = (ListView) view.findViewById(R.id.sensorsListView);
//
//
////
////        final RecyclerListAdapter adapter = new RecyclerListAdapter(getActivity(), this);
////
////        RecyclerView recyclerView = (RecyclerView) view;
////        recyclerView.setHasFixedSize(true);
////        recyclerView.setAdapter(adapter);
////        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
////
////        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
////        mItemTouchHelper = new ItemTouchHelper(callback);
////        mItemTouchHelper.attachToRecyclerView(recyclerView);
//    }

//    @Override
//    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
//        mItemTouchHelper.startDrag(viewHolder);
//    }
}
