package org.the_moufle.bdm.datapicmonitor.sensor;


public class  Sensor <T>
{
    private T   currentValue;
    private String  dataKey;
    private Double  limitMax;
    private Double  limitMin;
    private String  unitSymbol;
    private String  label;
    private Integer color;

    public T getCurrentValue() { return currentValue; }
    public void setCurrentValue(T currentValue) { this.currentValue = currentValue; }
    public String getDataKey() {
        return dataKey;
    }
    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }
    public Double getLimitMax() {
        return limitMax;
    }
    public void setLimitMax(Double limitMax) {
        this.limitMax = limitMax;
    }
    public Double getLimitMin() {
        return limitMin;
    }
    public void setLimitMin(Double limitMin) {
        this.limitMin = limitMin;
    }
    public String getUnitSymbol() {
        return unitSymbol;
    }
    public void setUnitSymbol(String unitSymbol) { this.unitSymbol = unitSymbol; }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public Integer getColor() { return color; }
    public void setColor(Integer color) { this.color = color; }

    public Sensor(
            String label,
            String dataKey,
            Double limitMin,
            Double limitMax,
            String unitSymbol,
            Integer color
    )
    {
        this.label      = label;
        this.dataKey    = dataKey;
        this.limitMin   = limitMin;
        this.limitMax   = limitMax;
        this.unitSymbol = unitSymbol;
        this.color      = color;
    }

    @Override
    public String toString()
    {
        return "Sensor{" +
                "currentValue=" + currentValue +
                ", dataKey='"   + dataKey + '\'' +
                ", limitMax="   + limitMax +
                ", limitMin="   + limitMin +
                ", unitSymbol=" + unitSymbol +
                ", label='"     + label + '\'' +
                ", color='"     + color +
                "}\n";
    }
}
